let nodemailer = require('nodemailer')
let fs = require('fs')

class EmailSender {
    constructor(elist) {
        this.emailList = elist
        this.transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'jon@betmix.com',
                pass: 'MyBetmix@68'
            }
        })
        this.mailOptions = {
            from: 'jon@betmix.com',
            to: this.emailList,
            subject: 'SSO Health Check',
            text: '',
            attachments: [{ filename: '', content: '' }],
        }
    }

    async promiseToSend(subject, message, attachFile) {
        return new Promise((resolve, reject) => {
            this.mailOptions.subject = subject
            this.mailOptions.text = message

            if (attachFile) {
                this.mailOptions.attachments[0].filename = attachFile
                this.mailOptions.attachments[0].content = fs.createReadStream(attachFile)
            }

            this.transporter.sendMail(this.mailOptions, (error, info) => {
                if (error) {
                    console.log(error)
                    reject(error)
                } else {
                    console.log('Email sent: ' + info.response)
                    resolve(true)
                }
            })
        });
    }
}
module.exports = EmailSender