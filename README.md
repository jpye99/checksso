## Table of Contents

-   [General info](#general-info)
-   [Notes](#notes)
-   [Technologies](#technologies)
-   [Setup](#setup)

## General info

This repo contains a node-javascript utility to verify SSO is functioning.

The intention is for the utility to be run repeatedly at intervals and if it sees a problem then send an email to a predefined email list, indicating the problem.

Developed as part of Jira story XD-1585

## Notes

1. the utility uses puppeteer, a chromium browser, to connect to the XPressbet site and login using an existing user's credentials

2. to run the utility as a single-shot:

```
> ssotool
```

3. The utility makes use of page-objects, developed as part of the `jestapi` project, which are copied here under the `pageobjects\mob\web` folder

4. The puppeteer browser is intended to be used in "headless:true" mode, but for development purposes you can set this to "false" and the chromium will display

5. Right now this utility uses no outside files for configuration, it hard-codes values for email-list and has defaults for the command line arguments

6. command line arguments can be displayed with

```
> ssotool --help
```

7. Command Line args are:

    | char | alternative        | value                                                     |
    | ---- | ------------------ | --------------------------------------------------------- |
    | -h   | --help             | output this usage information                             |
    | -w   | --warn <number>    | warning timeout for Auth0 login (default: 8)              |
    | -t   | --timeout <number> | timeout after which we have failed to login (default: 22) |
    | -m   | --email            | send email to a predefined list when an error occurs      |
    | -s   | --snap             | take a snapshot when an error occurs                      |
    | -u   | --user <uname>     | username to login as (default: "fredo")                   |
    | -p   | --pass <pword>     | password for login user (default: "Password1")            |
    | -d   | --url <dest-url>   | http address for product splash screen with login button  |

8. Example call

To use the hard-coded URL and email list and use the default usernameand password, but to change the warning time and the timeout time, to 5 seconds and 12 seconds, on Windows the call would be:

```
> ssotool -w 5  -t 12
```

9. to have logging output , the user needs to set the environment variable DEBUG to ssotool:

```
> set DEBUG=ssotool
```

10. on Linux, to set the logging and run:

```
>export DEBUG=ssotool
> ssotool -w 5
ssotool args: { startUrl: 'http://qa.xpressbetonline.com',
  ssotool   username: 'gI5rPnBDG3',
  ssotool   password: 'Xbet1234!',
  ssotool   msWarningTime: 5000,
  ssotool   msMaxTimeout: 22000,
  ssotool   version: '1.0' } +0ms
  ssotool at front page: http://qa.xpressbetonline.com +1s
  ssotool logging in to Auth0 as gI5rPnBDG3 / Xbet1234! +1s
  ssotool home page arrived at after 5102 ms +5s
  ssotool Error: Warning - login took 5102 (longer than 5000)
  ssotool     at action (/home/jonpye/ssotool/index.js:59:19)
  ssotool     at <anonymous>
  ssotool     at process._tickCallback (internal/process/next_tick.js:189:7) +0ms
Email sent: 250 2.0.0 OK  1574709606 v17sm9814878pfc.41 - gsmtp
  ssotool email sent to jonathan.pye@xpressbet.com +3s
```

## Technologies

The project is created with (see package.json for version numbers):

-   node js
-   puppeteer
-   nodemailer
-   debug
-   commander

## Setup

to install this utility:

1. ensure nodejs is installed
2. git-clone this repo, (or unzip the zip distribution)
3. install the dependent nodejs packages,( npm reads `package.json` to get the dependencies ):

```

> npm install

```

4. Link the tool name, ssotool, to the executable (index.js)

```
> npm link
```

5. now the utility is runnable from anywhere as:

```
> ssotool -w 5  -t 20
```
