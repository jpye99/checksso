let BasePanel = require('./BasePanel')
let HamburgerPanel = require('./HamburgerPanel')
    //let TrackPanel = require('./TrackPanel')

let selectors = {
    trackList: 'div#tracklistpage',
    trackSearch: '#tracksearch',
    breeds: '.ctitle',
    catTb: 'div.category.tb',
    thoroughbred: '.category.tb .cttxt',
    harness: '.category.harness .cttxt',
    international: '.category.inter .cttxt',
    tracks: 'div[bran="trak"]',
    hamburger: 'img.touchmenu',
    fundingTip: '.funding-tip',
    btnDeposit: 'button.biggreen.deposit',
}

class HomePanel extends BasePanel {

    constructor() {
        super(selectors.trackList)
        this.menuPopup = null
    }

    async clickThoroughbred() {
            await page.evaluate(() => {
                document.querySelector('.category.tb .cttxt').click();
            })
            await this.sleep(2000)
        }
        // need to know why this does not work...
        //async selectThoroughbred() {
        //    (await page.$$eval(selectors.breeds, a => a
        //        .filter(a => a.textContent === 'Thoroughbred')
        //    ))[0].click()
        //}
    async clickHamburger() {
        await page.evaluate(() => {
            document.querySelector('.touchmenu').click();
        })
    }
    async selectTrack(trackName) {
        try {
            await this.clickDivText(trackName)
                //let trackPanel = new TrackPanel()
                //await trackPanel.waitForPage()
                //await this.sleep(2000)
        } catch (ee) {
            throw new Error(ee.message)
        }
    }

    async signOut() {
        //await this.waitForPage()
        try {
            await this.clickHamburger()
            let hamburgerSlider = new HamburgerPanel();
            await hamburgerSlider.waitForPage();
            await hamburgerSlider.validate()

            await hamburgerSlider.logout()
        } catch (ee) {
            console.error(ee)
        }
    }
    async getBreeds() {
        let breeds = await page.$$(selectors.breeds)
        console.log("breeds: " + breeds.length)
        return breeds
    }

    async getFundingTipText() {
        return await this.parentDiv.$eval('.funding-tip', el => el.innerText);
    }

    async clickDeposit() {
        let depo = await page.$(selectors.btnDeposit)
        await depo.click()
    }

    async getCategories() {
        try {
            let twist = await page.$$('div.category')
            return await Promise.all(twist.map(async t => await t.$eval('div.ctitle', dd => dd.innerText)))

        } catch (ee) {
            throw new Error("failed in getCategories: " + ee.message)
        }
    }

    async closeHarness() {
        let twistie = await page.$('div.category.harness > .ctitle')
        await twistie.click()
        await page.waitFor(500)
    }

    async closeInternational() {
        let twistie = await page.$('div.category.inter > .ctitle')
        await twistie.click()
        await page.waitFor(500)
    }

    async clickFirstInternationalVideo() {
        let img = await page.$('div.category.inter div.video > img')
        await img.click()
    }

    async clickFirstInternationalTrack() {
        let tracks = await page.waitForSelector('div.category.inter div.tlists', { visible: true })
        let track = await tracks.$('div.tsection > div.list')
        let txt = await track.evaluate(el => el.getAttribute("tcod"))
        console.debug("text found: " + txt)
        await track.click()
        return txt
    }


    async closeCategory(catName) {
        try {
            let twist = await page.$$('div.category')
            let foundCat
            for (let elem of twist) {
                let name = await elem.$eval('div.ctitle', dd => dd.innerText)
                if (name === catName) {
                    console.log("found " + catName)
                    foundCat = elem
                    break
                }
            }
            if (foundCat) {
                await foundCat.click()
                await page.waitFor(500)
            }
        } catch (ee) {
            throw new Error(`failed to close ${catName}: ${ee.message}`)
        }
    }

    async openCategory(catName) {
            let tracks
            try {
                let twist = await page.$$('div.category')
                let foundCat
                for (let elem of twist) {
                    let name = await elem.$eval('div.ctitle', dd => dd.innerText)
                    if (name === catName) {
                        foundCat = elem
                        break
                    }
                }
                if (foundCat) {
                    await foundCat.click()
                    await page.waitFor(500)
                    tracks = foundCat.$eval('div.tlists', el => el.innerText)
                } else {
                    throw new Error("did not find category " + catName)
                }
                return tracks
            } catch (ee) {
                throw new Error("failed in clickCategory:" + ee.message)
            }
        }
        /*
async getDisplayedTracks() {
await page.waitForSelector('div.tlists', { visible: true })
let tracks = await page.$eval('div.tlists', el => el.innerText)
 
return tracks
}
*/
    async searchTrack(target) {
        try {
            await page.type(selectors.trackSearch, target)
            let opts = page.$$('div#trackoptions div')
            return opts
        } catch (ee) {
            throw new Error("failed in searchTrack:" + ee.message)
        }
    }

    async rightMenu() {
        await page.click(selectors.hamburger)
    }
}
module.exports = HomePanel