let BasePanel = require('./BasePanel')

const selectors = {
    buttonLogin: 'a[href$="login"]',
    btnSendUs: '.btn.btn-default',
    buttonHow: 'a[href$="how-to-bet"]',
    leftBurger: '#nav-toggle',
    xLogo: '#header-img',
}

//const newPagePromise = new Promise(x => browser.once('targetcreated', target => x(target.page())));

class FrontPanel extends BasePanel {

    constructor() {
        super(selectors.xLogo)
    }

    async clickLogin() {
        await Promise.all([
            page.waitForNavigation({ waitUntil: 'networkidle0' }),
            page.click(selectors.buttonLogin)
        ])
    }

    async clickSendUsAMessage() {
        await Promise.all([
            page.waitForNavigation({ waitUntil: 'networkidle0' }),
            page.click(selectors.btnSendUs)
        ])
    }

    async clickHamburger() {
        await page.evaluate(() => {
            document.querySelector('#nav-toggle').click();
        })
    }


}

module.exports = FrontPanel