let BasePanel = require('./BasePanel')

let selectors = {
    raceList: '#racelistpage',
    trackSelector: '#trackselector',
    btnChanges: '#changeslink',
    condition1: '.rlis-cond',
    condition2: '.rlis-cond2',
    races: 'div.racenumber',
    race1: 'div[rnum="1"]',
    raceTable: 'div.list',
    options: '#trackoptions',
}

class TrackPanel extends BasePanel {

    constructor() {
        super(selectors.raceList)
    }

    async selectTrack(newvalue) {
        await page.select('#trackselector select', newvalue)
    }

    async getSelectValue() {
            let eh = await page.$('#trackselector select')
            let value = await eh.getProperty("value")
            return await value.jsonValue()
        }
        /* this should have worked...
        async selectRaceNumber(raceNum) {
            (await page.$$eval('div.racenumber', (a, rnum) => a
                .filter(a => a.textContent === rnum), raceNum
            ))[0].click()
        }
        */
    async selectRace(raceNumber) {
        try {
            let raceTarget = await this.parentDiv.$(`div[rnum="${raceNumber}"]`)
            await raceTarget.click()
        } catch (ee) {
            console.error(ee)
            throw new Error(ee.message)
        }
    }

    async getRaces() {
        let races = []
        try {

            let parent = await this.parentDiv.$('#racelist')
            let list = await parent.$$('div.list')
                //console.log("numraces = " + list.length)
            for (let race of list) {
                let r = {}
                let msg = await race.evaluate(el => el.innerText)
                let txts = msg.split('\n')
                r.num = txts[0]
                r.name = txts[1]
                r.type = txts[2]
                r.status = txts[txts.length - 1]
                    /*
                    let tempo = await race.getProperty("rnum")
                    console.log("race num:" + tempo)
                    let tempo2 = await race.textContent
                    console.log("race text: " + tempo2)
                    r.num = tempo
                    r.desc = tempo2
                    */
                races.push(r)
            }
            return races
        } catch (ee) {
            throw new Error(ee.message)
        }
    }

}
module.exports = TrackPanel