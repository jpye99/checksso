let BasePanel = require('./BasePanel')

const selectors = {
    container: 'form.auth0-lock-widget',
    logo: 'img.auth0-lock-header-logo',
    titleHolder: 'div.auth0-lock-name',
    inputUsername: 'input[name="username"]',
    inputPassword: 'input[name="password"]',
    btnSubmit: 'button.auth0-lock-submit',
    errorMessage: 'div.auth0-global-message-error',
}

//const newPagePromise = new Promise(x => browser.once('targetcreated', target => x(target.page())));

class AuthPanel extends BasePanel {

    constructor() {
        super(selectors.container, "Auth0 Page")
    }


    async loginAs(un, pw, maxwait = 9000) {
        await page.waitForSelector(selectors.inputUsername, { visible: true })
        await page.type(selectors.inputUsername, un)
        await page.type(selectors.inputPassword, pw)
        let timer = {}
        timer.start = new Date
        await Promise.all([
            page.waitForNavigation({ waitUntil: 'load', timeout: maxwait }),
            page.click(selectors.btnSubmit)
        ])
        timer.end = new Date()
        return timer
    }

    async getLogoFileName() {
        let logoHolder = await page.$(selectors.logo)
        let txt = await logoHolder.evaluate(el => el.getAttribute("src"))
        return txt
    }

    async getTitle() {
        return await page.$eval(selectors.titleHolder, el => el.textContent)
    }

    async getUsernameValue() {
        await page.waitForSelector(selectors.inputUsername, { visible: true })
        return await page.$eval(selectors.inputUsername, el => el.value)
    }


}
module.exports = AuthPanel