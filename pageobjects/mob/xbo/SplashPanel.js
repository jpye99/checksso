let BasePanel = require('./BasePanel')

const selectors = {
    parent: '#ssosplash',
    message: 'div.cta',

    buttonLogin: 'button.login',
    buttonSignup: 'button.signup',

    csAssist: 'div.customerservice',
    problem: 'div.problemgambling',
}

//const newPagePromise = new Promise(x => browser.once('targetcreated', target => x(target.page())));

class SplashPanel extends BasePanel {

    constructor() {
        super(selectors.parent, "Splash Page")
    }

    async clickLogin() {
        let elem = await this.parentDiv.$(selectors.buttonLogin)
        await Promise.all([
            page.waitForNavigation({ waitUntil: 'networkidle0' }),
            elem.click()
        ])
    }


}

module.exports = SplashPanel