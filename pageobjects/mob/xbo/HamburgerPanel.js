let BasePanel = require('./BasePanel')

let selectors = {
    panelMenu: '#jPanelMenu-menu',
    accountText: '#accountbox',
    cogWheel: 'div.cog.simplemenuitem',
    accountNumber: '#accountbox span.actn',
    balance: '#balancemenu',
    buttonDeposit: 'button.biggreen.deposit',
    linkMenu: '.simplemenuitem',
    menuWrapper: 'div.menuwrapper',
}

class HamburgerPanel extends BasePanel {

    constructor() {
        super(selectors.panelMenu)
    }

    async logout() {
        /*
        let links = await this.parentDiv.$$(selectors.linkMenu)
        let foundLogout = false
        for (let link of links) {
            let txt = await link.textContent
            if (txt === 'Log Out') {
                foundLogout = true
                await link.click()
            }
        }
        if (!foundLogout) {
            throw new Error("did not see the Log Out link")
        }
        */
        let menus = await page.$('div.menuwrapper')
        let lout = await menus.$x('./div[contains(text(), "Log Out")]')
        if (lout.length) {
            console.log("lout count: " + lout.length)
        } else {
            console.log("no liut dice")
        }
        //await this.clickDivText("Log Out")
        //(await this.parentDiv.$$eval('.simplemenuitem', a => a
        //    .filter(a => a.textContent === "Log Out")
        //))[0].click()
    }

    /*
        async logout() {
            try {
                let divs = await this.parentDiv.$x('./div/div[contains(text(),"Log Out")]')
                if (divs.length > 0) {
                    await divs[0].click();
                } else {
                    throw new Error("did not find Log Out div")
                }
            } catch (ee) {
                throw new Error("failed to logout from Hamburger:" + ee)
            }
        }
    */
    async deposit() {
        await page.click(selectors.buttonDeposit)
        await page.waitFor(3000)
            // ...
    }

    async getAccountNumber() {
        return await this.parentDiv.$eval('#accountbox', el => el.innerText);
    }

    async getBalance() {
        return await this.parentDiv.$eval('#balancemenu', el => el.innerText);
    }

    async getLinks() {
        let links = this.parentDiv.$$(selectors.linkMenu)
    }
}
module.exports = HamburgerPanel