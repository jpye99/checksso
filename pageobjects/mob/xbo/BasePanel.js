class BasePanel {
    constructor(selector, pagename = "Page") {
        this.selector = selector
        this.parentDiv = null
        this.pageName = pagename
    }

    async waitForPage() {
        try {
            await page.waitForSelector(this.selector)
        } catch (e) {
            throw new Error("Not displaying expected page - " + e)
        }
    }
    async validate(tout = 8000) {
        try {
            this.parentDiv = await page.waitForSelector(this.selector, { visible: true, timeout: tout })
        } catch (ee) {
            throw new Error("Cannot validate the " + this.pageName + ": " + this.selector + ":" + ee.message + "::" + tout)
        }
    }

    sleep(milliseconds) {
        // should never have to use this.
        return new Promise(resolve => setTimeout(resolve, milliseconds))
    }

    async clickByText(text) {
        //this.utils.clickByText(page, text)
    }

    async clickButton(labelText) {
        let buttons = await page.$x('//button[contains(., labelText)]')
        if (buttons) {
            await buttons[0].click()
        } else {
            throw new Error("did not find a button with text " + labelText)
        }
    }

    async clickDivText(dtext) {
        try {
            let divs = await page.$x(`//div[contains(text(),"${dtext}")]`)
            if (divs.length > 0) {
                await divs[0].click();
            } else {
                throw new Error("did not find div text: " + dtext)
            }
        } catch (ee) {
            //console.error(ee.message)
            throw new Error(ee)
        }
    }

    async mobileHamburger() {
        await page.evaluate(() => {
            document.querySelector('img.touchmenu').click();
        })
    }

    async mobileBack() {
        await page.evaluate(() => {
            document.querySelector('img.backarrow').click();
        })
    }

    async mobileLogout() {
        await this.mobileHamburger()
        let wrapper = await page.$('.menuwrapper')
        let wrappedItems = await wrapper.$$('.simplemenuitem')
        await page.waitFor(1000)
        for (let linko of wrappedItems) {
            let tt = await linko.evaluate(node => node.textContent)
            if (tt.trim() === 'Log Out') {
                await linko.focus()
                await page.keyboard.press('Enter')
                break
            }
        }
        console.debug("BasePanel::pressed Log Out")
        await page.waitFor(1000)
    }

}
module.exports = BasePanel