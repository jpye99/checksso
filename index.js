#!/usr/bin/env node

const debug = require('debug')
const puppeteer = require('puppeteer');
const cmdline = require('commander')
const devices = require('puppeteer/DeviceDescriptors');

let FrontPanel = require('./pageobjects/mob/xbo/FrontPanel')
let AuthPanel = require('./pageobjects/mob/xbo/AuthPanel')
let HomePanel = require('./pageobjects/mob/xbo/HomePanel')
let TrackPanel = require('./pageobjects/mob/xbo/TrackPanel')
let ArgSetter = require('./args')
let EmailSender = require('./emailer')

const device = devices['iPhone X'];
let parties = 'jonathan.pye@xpressbet.com'

let subjectText = "SSO Check for Xpressbet mobile browser"
let fileName = "./xbo.png"
let log = debug('ssotool')
let version = '1.1'


let emailer = new EmailSender(parties)
let argSetter = new ArgSetter(version)
let frontPanel = new FrontPanel()
let authPanel = new AuthPanel()
let homePanel = new HomePanel()
let result = "OK"


const action = async() => {

    const args = argSetter.readArgs()
    log("args: %O", args)

    try {
        browser = await puppeteer.launch({
            headless: true,
            args: ['--no-sandbox', '--disable-setuid-sandbox']
        });
        page = await browser.newPage();
        await page.emulate(device);
        await page.goto(args.startUrl);
        await frontPanel.validate()
        log("at front page: %s", args.startUrl)
        await frontPanel.clickLogin()
        result = "SSO FAILURE - XBO Mobile is not getting to the Auth0 sign-in display"

        await authPanel.validate()
        result = `SSO FAILURE - XBO mobile timed out after ${args.msMaxTimeout} ms`
        log("logging in to Auth0 as %s / %s", args.username, args.password)

        let timing = await authPanel.loginAs(args.username, args.password, args.msMaxTimeout)
        await homePanel.validate()
        log("timing: %o", timing)
        let timeTaken = timing.end - timing.start

        log("login complete after %d ms", timeTaken)
        if (timeTaken > args.msWarningTime) {
            result = `SSO WARNING - XBO mobile login took ${timeTaken}ms (longer than ${args.msWarningTime})`
            throw new Error(`Warning - login took ${timeTaken} (longer than ${args.msWarningTime})`)
        }
        result = "SSO - XBO mobile login is OK (" + timeTaken + "ms)"
    } catch (ee) {
        log(ee)
        if (args.snap || args.email) {
            await page.screenshot({ path: fileName });
        }
        if (args.email) {
            try {
                await emailer.promiseToSend(result, `${ee.message}`, fileName)
                log("email sent to %s", parties)
            } catch (ee2) {
                log(ee2)
            }
        }
    } finally {
        console.log(new Date().toISOString() + "::" + result)
        await browser.close();
    }
}

action()