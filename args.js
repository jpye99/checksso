const cmdline = require('commander')

class ArgSetter {
    constructor(ver) {
        this.args = {
            startUrl: 'http://qa.xpressbetonline.com',
            username: 'gI5rPnBDG3',
            password: 'Xbet1234!',
            msWarningTime: 6000,
            msMaxTimeout: 12000,
            version: ver,
        }
    }

    setFrequency(newValue, prev) {
        return newValue ? newValue : prev
    }

    setWarningTime(newValue, prev) {
        return newValue ? newValue : prev
    }

    setTimeoutTime(newValue, prev) {
        return newValue ? newValue : prev
    }

    setUrl(newValue, prev) {
        return newValue ? newValue : prev
    }

    setUsername(newValue, prev) {
        return newValue ? newValue : prev
    }

    setPassword(newValue, prev) {
        return newValue ? newValue : prev
    }

    readArgs() {
        cmdline
            .option('-u, --user <uname>', 'username to login as ', this.setUsername, `${this.args.username}`)
            .option('-m, --email', 'send email on warning or error condition')
            .option('-s, --snap', 'take snapshot of device screen on error condition')
            .option('-p, --pass <pword>', 'password for login user', this.setPassword, `${this.args.password}`)
            .option('-w, --warn <number>', 'warning timeout (secs)for Auth0 login', this.setWarningTime, 8)
            .option('-t, --timeout <number>', 'timeout (secs) after which we have failed to login', this.setTimeoutTime, 22)
            .option('-d, --url <dest-url>', 'http address for product splash screen with login button',
                this.setUrl, `${this.args.startUrl}`)

        cmdline.parse(process.argv)
        this.args.startUrl = cmdline.url
        this.args.msMaxTimeout = cmdline.timeout * 1000
        this.args.msWarningTime = cmdline.warn * 1000
        this.args.username = cmdline.user
        this.args.password = cmdline.pass
        this.args.email = cmdline.email
        this.args.snap = cmdline.snap
        return this.args
    }

}
module.exports = ArgSetter